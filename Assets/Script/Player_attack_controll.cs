﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player_attack_controll : MonoBehaviour {

    private Rigidbody2D rb;
    private Player_parameter pp;
    private Player_Anim_Event p_anim;

    // Use this for initialization
    void Start () {

        rb = GameObject.Find("Player_Parent").GetComponent<Rigidbody2D>();
        pp = GameObject.Find("Player_Parent").GetComponent<Player_parameter>();
        p_anim = GetComponentInParent<Player_Anim_Event>();
    }

    // Update is called once per frame
    void Update () {


    }

    //敵がコライダに接触したらダメージ処理
    private void OnTriggerEnter2D(Collider2D col)
    {
        string layerName = LayerMask.LayerToName(col.gameObject.layer);
        




        if (layerName == "Enemy" || layerName == "Dead")
        {
            if(p_anim.attack_status == 1)
            {
                col.GetComponentInParent<Damageable>().Damage((int)Enum_AttackType.JUST, pp.atk, rb.velocity.magnitude);
            }
            else
            {
                col.GetComponentInParent<Damageable>().Damage((int)Enum_AttackType.NORMAL, pp.atk, rb.velocity.magnitude);

            }


        }

    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Effect_controll : MonoBehaviour {

    public float clear_distance = 100.0f;   //Inspectorで設定（エフェクトが表示され始める距離）
    public float effect_scale = 20.0f;   //Inspectorで設定（エフェクトのスケール率）
    private GameObject gm;
    private float temp_effect_scale = 1.0f;
    private float temp_alpha;
    

    // Use this for initialization
    void Start () {
        gm = GameObject.Find("GameManager");

    }

    // Update is called once per frame
    void Update () {

        //エフェクトを回転
        transform.Rotate(new Vector3(0, 0, 10));

        //敵との最至近距離を踏まえてエフェクトスケールを決定
        temp_effect_scale = gm.GetComponent<Game_Manager>().near_enemy_distance / effect_scale;

        //エフェクトをスケーリング
        transform.localScale = new Vector3(temp_effect_scale, temp_effect_scale, temp_effect_scale);

        //
        temp_alpha = 1.0f - (gm.GetComponent<Game_Manager>().near_enemy_distance / clear_distance);
        
        this.GetComponent<SpriteRenderer>().color = new Color(this.GetComponent<SpriteRenderer>().color.r, this.GetComponent<SpriteRenderer>().color.g, this.GetComponent<SpriteRenderer>().color.b, temp_alpha);


    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Damageable
{
    void Damage();
    void Damage(int type, int atk, float speed);

}


public enum Enum_MoveDir
{
    LEFT = -1,
    STAY,
    RIGHT
}


public enum Enum_AttackType
{
    JUST = 0,
    NORMAL,
    CHASE
}

public static class Define
{
    public const float attack_just_rate = 3.0f;
    public const float attack_normal_rate = 1.0f;
    public const float attack_chase_rate = 0.5f;

}



public class Define_etc : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy01_controll : MonoBehaviour,Damageable {
    public Rigidbody2D rb;
    public float speed_rate = 0.01f;
    public float blow_rate = 30.0f;
    public LayerMask mask;
    public GameObject effect_hit_prefab;        //Inspectorで設定
    public float damage_ui_offset = 2.0f;
    private Animator child_animator;
    private bool survive = true;
    private GameObject gm;
    private Enemy01_parameter ep;
    private float temp_mag;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        child_animator = GetComponentInChildren<Animator>();
        gm = GameObject.Find("GameManager");
        ep = GetComponent<Enemy01_parameter>();

    }

    // Update is called once per frame
    void Update () {

        if (survive)
        {
            rb.AddForce(Vector2.left * Time.deltaTime * speed_rate, ForceMode2D.Impulse);
        }
    }


    private void OnTriggerEnter2D(Collider2D col)
    {
        string layerName = LayerMask.LayerToName(col.gameObject.layer);

        if (layerName == "Player")
        {
            col.GetComponentInParent<Damageable>().Damage((int)Enum_AttackType.NORMAL, ep.atk, rb.velocity.magnitude);

        }
    }
    

    //攻撃した側のオブジェクトから呼び出される
    public void Damage ()
    {


    }

    public void Damage(int type, int atk, float speed)
    {
        //ダメージリアクション呼び出し
        child_animator.SetTrigger("Down");

        //直前のスピードを保存（ダメージに反映）
        temp_mag = rb.velocity.magnitude;

        //速度リセット
        rb.velocity = new Vector2(0, 0);

        //吹き飛び
        rb.AddForce(Vector2.right * blow_rate * speed_rate, ForceMode2D.Impulse);

        //生存フラグをオフ
        survive = false;

        //コライダのレイヤー名を変更
        this.gameObject.layer = LayerMask.NameToLayer("Dead");

        //追撃モードを設定
        gm.GetComponent<Game_Manager>().chase_mode_state = 1;

        //ヒットエフェクトを表示
        Instantiate(effect_hit_prefab, this.gameObject.transform);
        
        if(type == (int)Enum_AttackType.JUST)
        {
            //ダメージUIを呼び出し
            gm.GetComponent<Game_Manager>().Damage_UI_ON((int)(atk * (speed + temp_mag) * Define.attack_just_rate) + atk, this.transform.position, damage_ui_offset, type);

        }
        else if(type == (int)Enum_AttackType.NORMAL)
        {
            //ダメージUIを呼び出し
            gm.GetComponent<Game_Manager>().Damage_UI_ON((int)(atk * (speed + temp_mag) * Define.attack_normal_rate) + atk, this.transform.position, damage_ui_offset, type);

        }

    }

}

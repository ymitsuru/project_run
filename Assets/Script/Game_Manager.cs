﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Game_Manager : MonoBehaviour {

    public Canvas fade_canvas;      //Inspectorで設定
    public Canvas Main_canvas;      //Inspectorで設定
    public Canvas Damage_UI_canvas;      //Inspectorで設定
    public GameObject Player_obj;   //Inspectorで設定
    public GameObject[] Enemy_array;
    public float near_enemy_distance = 100000.0f;
    public int chase_mode_state = 0;        //追撃モード状態
    public float chase_time = 1.5f;         //追撃モードの時間設定
    public float slow_start_change_time = 0.1f;   //スロー開始時の遷移時間
    public float slow_end_change_time = 0.3f;   //スロー終了時の遷移時間
    public float fade_start_change_time = 0.1f;   //暗転開始時の遷移時間
    public float fade_end_change_time = 0.3f;   //暗転終了時の遷移時間
    public float slow_rate = 0.2f;          //スロー度合い
    public float goal_distance = 10000.0f;  //ゴールまでの距離
    public Text tx_player_speed;        //Inspectorで設定
    public Text tx_player_distance;     //Inspectorで設定
    public Text tx_time_limit;     //Inspectorで設定
    private float temp_chase_time = -1.0f;   //追撃モードの残り時間
    private float temp_enemy_distance;
    private float temp_player_pos;
    public float temp_time_limit = 60.0f;
    private float player_move_distance = 0.0f;
    private Rigidbody2D player_rb;
    private bool slow_start_flag = false;    //この値がtrueなら、スロー状態に移行し続ける
    private bool slow_end_flag = false;    //この値がtrueなら、スローから通常に戻し続ける

    // Use this for initialization
    void Start() {

        //敵オブジェクトのリストを取得
        Enemy_array = GameObject.FindGameObjectsWithTag("Enemy");

        //プレイヤーのリジボ参照を取得
        player_rb = Player_obj.GetComponentInChildren<Rigidbody2D>();

        //プレイヤーのｘ座標を保持
        temp_player_pos = Player_obj.gameObject.transform.position.x;

    }

    // Update is called once per frame
    void Update () {

        //時間経過を反映
        temp_time_limit -= Time.deltaTime;
        tx_time_limit.text = "残り" + temp_time_limit.ToString("N1") + " 秒";



        //プレイヤーの移動距離を格納
        goal_distance -= Player_obj.gameObject.transform.position.x - temp_player_pos;

        temp_player_pos = Player_obj.gameObject.transform.position.x;

        //画面のUIに反映
        tx_player_distance.text = "救難地点まで " + goal_distance.ToString("N0") + " m";


        //一番近い敵との距離を算出する処理前の初期化
        temp_enemy_distance = 10000.0f;    

        for (int i = 0; i < Enemy_array.Length; i++)
        {
            //レイヤー名がデッドになっていないものから
            if(LayerMask.LayerToName(Enemy_array[i].layer) != "Dead")
            {
                //現在の最至近距離の敵よりも近かったら
                if (temp_enemy_distance > Enemy_array[i].transform.position.x - Player_obj.transform.position.x)
                {
                    //その距離を最至近距離として上書き
                    temp_enemy_distance = Enemy_array[i].transform.position.x - Player_obj.transform.position.x;

                }
            }
        }

        //生きている最も近い敵との距離を格納
        near_enemy_distance = temp_enemy_distance;

        //プレイヤーの速度をテキストに格納(3.6掛けはｍ/秒からの時速換算)
        tx_player_speed.text = (player_rb.velocity.x * 3.6f).ToString("N0") + " km/h";


        //追撃モード開始
        if(chase_mode_state == 1 && temp_chase_time <= 0.0f)
        {
            //追撃モードの持続時間を設定
            temp_chase_time = chase_time;

            //暗転・スローオン
            BlackOut_ON();
            Slow_ON();
        }
        //追撃モード中の処理
        else if(chase_mode_state == 1 && temp_chase_time > 0.0f)
        {
            temp_chase_time -= Time.deltaTime * (1.0f / slow_rate);

            //指定の時間が経過したら
            if (temp_chase_time < 0.0f)
            {
                //追撃モード終了
                chase_mode_state = 0;

                //暗転・スローオフ
                BlackOut_OFF();
                Slow_OFF();
            }

        }


        //スロー開始遷移
        if (slow_start_flag == true
            && Time.timeScale > slow_rate
            && Time.timeScale != 0.0f)
        {
            //スロー値が範囲外にならないように（0～100）
            if ((Time.timeScale - ((1.0f / slow_start_change_time) * Time.deltaTime)) < slow_rate)
            {
                Time.timeScale = slow_rate;
                slow_start_flag = false;
            }
            else
            {
                Time.timeScale -= (1.0f / slow_start_change_time) * Time.deltaTime;
            }
        }
        
        //スロー終了遷移
        if (slow_end_flag == true
            && Time.timeScale < 1.0f
            && Time.timeScale != 0.0f)
        {
            //スロー値が範囲外にならないように（0～100）
            if ((Time.timeScale + ((1.0f / slow_end_change_time) * Time.deltaTime)) > 1.0f)
            {
                Time.timeScale = 1.0f;
                slow_end_flag = false;
            }
            else
            {
                Time.timeScale += (1.0f / slow_end_change_time) * Time.deltaTime;
            }
        }

    }

    public void BlackOut_ON()
    {
        //キャンバスをオン
        fade_canvas.enabled = true;

        //指定時間かけて暗転
        fade_canvas.GetComponentInChildren<Image>().CrossFadeAlpha(1.0f, fade_start_change_time, false);
    }

    public void BlackOut_OFF()
    {
        //指定時間かけて暗転明け
        fade_canvas.GetComponentInChildren<Image>().CrossFadeAlpha(0.0f, fade_end_change_time,false);

    }

    // アニメーションイベント：スローオン
    public void Slow_ON()
    {
        //スロー設定
        slow_start_flag = true;

    }

    // アニメーションイベント：スローオフ
    public void Slow_OFF()
    {
        //スロー戻しフラグをオン
        slow_end_flag = true;

    }


    // ダメージUI表示
    public void Damage_UI_ON(int damage, Vector3 pos, float y_offset, int color)
    {
        //呼び出し元によってUIを表示する位置を調整
        pos.y += y_offset;

        //プレハブから生成
        Canvas obj = Instantiate(Damage_UI_canvas, pos, Quaternion.identity);

        //表示するダメージを格納
        obj.GetComponentInChildren<UI_damage_controll>().dmg = damage;

        if(color == (int)Enum_AttackType.JUST)
        {
            obj.GetComponentInChildren<Text>().color = Color.red;

        }
        else if(color == (int)Enum_AttackType.NORMAL)
        {
            obj.GetComponentInChildren<Text>().color = Color.yellow;

        }


    }
    
}

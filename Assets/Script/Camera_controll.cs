﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_controll : MonoBehaviour {

    Transform player_trans;
    public float player_camera_pos = 9f;

	// Use this for initialization
	void Start () {

        //プレイヤーの座標参照を取得
        player_trans = GameObject.Find("Player_Parent").transform;
        
	}
	
	// Update is called once per frame
	void Update () {

        //プレイヤーの位置に合わせてカメラ位置を追従
        transform.position = new Vector3(player_trans.position.x + player_camera_pos, transform.position.y, transform.position.z);




    }
}

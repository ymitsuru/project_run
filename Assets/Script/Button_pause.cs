﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button_pause : MonoBehaviour {

    private float save_time_scale = 1.0f;
    private bool pause_on = false;
    public GameObject Text_pause;   //Inspectorで設定
    public GameObject Panel_pause;   //Inspectorで設定
    
    // Use this for initialization
    void Start () {
		

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        if (!pause_on)
        {
            pause_on = true;
            save_time_scale = Time.timeScale;
            Time.timeScale = 0.0f;

            Text_pause.GetComponent<Text>().enabled = true;
            Panel_pause.GetComponent<Image>().enabled = true;

        }
        else
        {
            pause_on = false;
            Time.timeScale = save_time_scale;

            Text_pause.GetComponent<Text>().enabled = false;
            Panel_pause.GetComponent<Image>().enabled = false;

        }

    }




}

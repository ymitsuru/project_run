﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy01_Anim_Event : MonoBehaviour {

    private GameObject gm;
    public float slow_rate = 0.2f;


    // Use this for initialization
    void Start () {

        gm = GameObject.Find("GameManager");

    }

    // Update is called once per frame
    void Update () {
		
	}




    // アニメーションイベント：スローオン
    public void Slow_ON()
    {
        //スロー設定
        gm.GetComponent<Game_Manager>().Slow_ON();

        //暗転オン
        gm.GetComponent<Game_Manager>().BlackOut_ON();
    }

    // アニメーションイベント：スローオフ
    public void Slow_OFF()
    {
        //スロー設定戻し
        gm.GetComponent<Game_Manager>().Slow_OFF();

        //暗転オフ
        gm.GetComponent<Game_Manager>().BlackOut_OFF();

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Anim_Event : MonoBehaviour {

    public float dash_velocity = 10.0f;
    public float attack_dash_velocity = 5.0f;
    private Rigidbody2D rb;
    private GameObject gm;
    public float slow_rate = 0.2f;
    public AudioSource SE_swing;    //Inspectorで設定
    public int attack_status = 0;   //アニメーション側で遷移

    // Use this for initialization
    void Start()
    {
        rb = GetComponentInParent<Rigidbody2D>();
        gm = GameObject.Find("GameManager");
    }

    // Update is called once per frame
    void Update() {


    }

    // アニメーションイベント：加速
    public void Dash_Play()
    {
        //急加速
        rb.AddForce(Vector2.right * dash_velocity, ForceMode2D.Impulse);
    }


    // アニメーションイベント：斬り加速
    public void Attack_Dash()
    {
        //加速
        rb.AddForce(Vector2.right * attack_dash_velocity, ForceMode2D.Impulse);
    }

     // アニメーションイベント：スローオン
    public void Slow_ON()
    {
        //スロー設定
        gm.GetComponent<Game_Manager>().Slow_ON();

        //暗転オン
        gm.GetComponent<Game_Manager>().BlackOut_ON();
    }

    // アニメーションイベント：スローオフ
    public void Slow_OFF()
    {
        //スロー設定戻し
        gm.GetComponent<Game_Manager>().Slow_OFF();

        //暗転オフ
        gm.GetComponent<Game_Manager>().BlackOut_OFF();

    }


    // アニメーションイベント：ダメージ中オン
    public void Damage_ON()
    {
        //レイヤー名をダメージ中に
        rb.gameObject.layer = LayerMask.NameToLayer("Player_damage");
    }

    // アニメーションイベント：ダメージ中オフ
    public void Damage_OFF()
    {
        //レイヤー名をプレイヤーに戻し
        rb.gameObject.layer = LayerMask.NameToLayer("Player");

    }


    // アニメーションイベント：加速度リセット
    public void Force_Reset()
    {
        //プレイヤーの可変加速度をリセット
        rb.GetComponentInParent<Player_controll>().now_speed_rate = 0;
    }

    // スウィングSEを再生
    public void Play_SE_Swing()
    {
        SE_swing.PlayOneShot(SE_swing.clip);

    }


}

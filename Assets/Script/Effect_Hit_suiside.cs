﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect_Hit_suiside : MonoBehaviour {

    public float life_time = 0.02f;
    public float fade_in_speed = 30.0f;
    public float fade_out_speed = 10.0f;
    private float now_time;
    private SpriteRenderer sr;
    private bool fade_in_flag = true;
    private bool fade_out_flag = false;
    private float temp_alpha;
    private Vector3 temp_scale;
    public float scale_rate_x = 50.0f;
    public float scale_rate_y = 50.0f;
    public float scale_rate_z = 50.0f;
    public float offset_range = 0.5f;
    public float scale_range = 0.2f;

    // Use this for initialization
    void Start () {

        //寿命を設定
        now_time = life_time;
        
        sr = this.gameObject.GetComponent<SpriteRenderer>();

        //最初は透明
        sr.color = new Vector4(sr.color.r, sr.color.g, sr.color.b, 0.0f);
        temp_alpha = sr.color.a;
        
        //ランダムにスケールを縮めてスタート
        temp_scale = new Vector3(Random.Range(0.0f,scale_range), Random.Range(0.0f, scale_range), Random.Range(0.0f, scale_range));
        this.transform.localScale = temp_scale;

        //ランダムに回転
        this.transform.rotation = Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f));

        //ランダムにずらす
        this.transform.position += new Vector3(Random.Range(-offset_range,offset_range), Random.Range(-offset_range, offset_range),0.0f);

        this.transform.parent = null;

    }

    // Update is called once per frame
    void Update () {

        //最初にフェードイン
        if (fade_in_flag)
        {
            temp_alpha += fade_in_speed * Time.deltaTime;

            if (temp_alpha >= 1.0f)
            {
                temp_alpha = 1.0f;
                fade_in_flag = false;

            }

            sr.color = new Vector4(sr.color.r, sr.color.g, sr.color.b, temp_alpha);

            //スケーリング（一気に拡大）
            temp_scale.x += scale_rate_x * Time.deltaTime;
            temp_scale.y += scale_rate_y * Time.deltaTime;
            temp_scale.z += scale_rate_z * Time.deltaTime;
            this.transform.localScale = temp_scale;

        }
        //最後にフェードアウト
        else if (fade_out_flag)
        {

            //スケーリング（ちょっと縮みつつ）
            temp_scale.x -= scale_rate_x / 10 * Time.deltaTime;
            temp_scale.y -= scale_rate_y / 10 * Time.deltaTime;
            temp_scale.z -= scale_rate_z / 10 * Time.deltaTime;
            this.transform.localScale = temp_scale;

            temp_alpha -= fade_out_speed * Time.deltaTime;

            if (temp_alpha <= 0.0f)
            {
                temp_alpha = 0.0f;
                fade_out_flag = false;
                Destroy(this.gameObject);
            }

            sr.color = new Vector4(sr.color.r, sr.color.g, sr.color.b, temp_alpha);

        }
        //フェードしていないとき
        else
        {
            now_time -= Time.deltaTime;

            if (now_time < 0.0f)
            {
                fade_out_flag = true;

            }

        }


    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_UI_controll : MonoBehaviour {

    private Game_Manager gm;
    private Text tx;

    // Use this for initialization
    void Start () {
        gm = GameObject.Find("GameManager").GetComponent<Game_Manager>();
        tx = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update () {

        tx.text = gm.near_enemy_distance.ToString("N0") + " m▼";        
            
	}
}
